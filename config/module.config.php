<?php

use Api\QueryBuilder\Filter\ORM\RegexpCount;
use Doctrine\ORM\Mapping\Driver\XmlDriver;
use Doctrine\Persistence\Mapping\Driver\MappingDriverChain;
use Laminas\ServiceManager\Factory\InvokableFactory;

return [
    'doctrine' => [
        'driver' => [
            'orm_default_xml_driver' => [
                'class' => XmlDriver::class,
                'cache' => 'array',
                'paths' => [
                    0 => __DIR__ . '/../src/Api/V1/Entity/Db/Mapping',
                ],
            ],
            'orm_default' => [
                'class' => MappingDriverChain::class,
                'drivers' => [
                    'Api\\V1\\Entity\\Db' => 'orm_default_xml_driver',
                ],
            ],
        ],
    ],

    'api-tools-doctrine-querybuilder-filter-orm' => [
        'aliases' => [
            'regexpcount' => RegexpCount::class,
        ],
        'factories' => [
            RegexpCount::class => InvokableFactory::class,
        ],
    ],


];