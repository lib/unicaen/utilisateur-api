swagger: '2.0'
#host: utilisateur-api.unicaen.fr
#schemes:
#  - https
info:
  title: UnicaenUtilisateur Api
  description: "Les web services UnicaenUtilisateur.

    - Note : une version 'light' est disponible pour la plus part des entités.
  
    - Ces versions light donne les identifiants des objets enfants et non les objets eux mêmes
    
    - Les appels ne sont pas données ici pour ne pas surchargé la documentation mais peuvent être utilisé comme leurs versions 'normal'. Ie : GET - /api/user-light/4
    
  "
  version: '1.0'

securityDefinitions:
  basicAuth:
    type: basic

security:
  - basicAuth: []

produces:
  - application/hal+json

parameters:
  triFieldsParam:
    name: order-by
    in: query
    description: Tri des résultats en indiquant le champ (et l'ordre de tri séparés par une virgule)
    type: string
    collectionFormat: csv

paths:
  /api/user:
    get:
      summary: Rechercher des utilisateurs
      description: |
        Commande permettant de rechercher des utilisateur à partir de différents paramètres.
        
        #### Description des données renvoyées

        | Attribut          | Définition                              |
        |-------------------|-----------------------------------------|
        | **id**            | ID de l'utilisateur                     |
        | **username**      | Login de l'utilisateur                  |
        | **displayName**   | Nom afficher dans l'application         |
        | **email**         | Adresse mail de l'utilisateur           |
        | **state**         | Etat de l'utilisateur                   |
        | **lastRole**      | Dernier rôle utilisé                    |
        | **roles**         | Liste des roles de l'utilisateur        |

      tags:
        - User
      parameters:
        - name: id
          in: query
          description: id de l'utilisateur
          type: integer
        - name: username
          in: query
          description:  Login de l'utilisateur
          type: string
        - $ref: '#/parameters/triFieldsParam'
      responses:
        200:
          description: Liste des utilisateurs
          schema:
            type: array
            items:
              $ref: '#/definitions/User'
        400:
          description: Erreur. Requête mal formée
          schema:
            $ref: '#/definitions/Error'

    post:
      summary: Ajout d'un ou plusieurs
      description: |
        Commande permettant d'ajouter un ou plusieurs utilisateurs
         Les commits en BDD sont désactivées. La méthode ne sert que d'exemple.
        #### Description du json à envoyer

        | Attribut          | Définition                              |
        |-------------------|-----------------------------------------|
        | **id**            | ID de l'utilisateur                     |
        | **username**      | Login de l'utilisateur                  |
        | **displayName**   | Nom afficher dans l'application         |
        | **email**         | Adresse mail de l'utilisateur           |
        | **state**         | Etat de l'utilisateur                   |

      tags:
        - User
      #On doit fournir les données en JSON
      consumes:
        - application/json
      parameters:
        - in: body
          name: data
          description: "json contenant les données d'un/des utilisateurs à creer.
          on peut également fournir un seul utilisateur sans passer par un tableau mais directement l'objet json.
        "
        # TODO : voir comment passer en Swager 3.0 et utiliser l'option OneOf
        #
          schema:
            type: array
            items:
              type: object
              required:
                - username
                - displayName
              properties:
                id:
                  type: integer
                username:
                  type: string
                displayName:
                  type: string
                email:
                  type: string
                state:
                  type: boolean
      responses:
        200:
          description: Liste des utilisateurs ajoutées
          schema:
            type: array
            items:
              $ref: '#/definitions/User'
        400:
          description: Erreur. Requête mal formée
          schema:
            $ref: '#/definitions/Error'

    patch:
      summary: Modification des utilisateurs
      description: |
        Commande permettant de modifier des utilisateurs
        Il ne s'agit que d'un exemple d'utilisation, les modifications du username ne sont pas prise en compte
  
        #### Description des éléments json à envoyer

        | Attribut          | Définition                              |
        |-------------------|-----------------------------------------|
        | **id**            | ID de l'utilisateur                     |
        | **username**      | Login de l'utilisateur                  |
        | **displayName**   | Nom afficher dans l'application         |
        | **email**         | Adresse mail de l'utilisateur           |
        | **state**         | Etat de l'utilisateur                   |

      tags:
        - User
      consumes:
        - application/json
      parameters:
        - in: body
          name: data
          description: "json contenant les données d'un/des utilisateurs à modifier
            Pour les exemples sans risque, la modifications du username ne sont pas prise en compte
        "
          schema:
            type: array
            items:
              type: object
              required:
                - id
              properties:
                id:
                  type: integer
                username:
                  type: string
                displayName:
                  type: string
                email:
                  type: string
                state:
                  type: boolean
      responses:
        200:
          description: Liste des utilisateurs modifié
          schema:
            type: array
            items:
              $ref: '#/definitions/User'
        400:
          description: Erreur. Requête mal formée
          schema:
            $ref: '#/definitions/Error'

    delete:
      summary: Suppression des utilisateurs
      description: |
        Commande permettant de supprimer des utilisateurs. 
        Il ne s'agit que d'un exemple, les modifications en bdd sont désactivées
        
        #### Description des éléments json à envoyer

        | Attribut          | Définition                              |
        |-------------------|-----------------------------------------|
        | **id**            | ID de l'utilisateur                     |

      tags:
        - User
      consumes:
        - application/json
      parameters:
        - in: body
          name: data
          description: "json contenant les données des utilisateurs à modifier"
          schema:
            type: array
            items:
              type: object
              required:
                - id
              properties:
                id:
                  type: integer
      responses:
        200:
          description: Success
        400:
          description: Erreur. Requête mal formée
          schema:
            $ref: '#/definitions/Error'

  /api/user/{user_id}:
    get:
      summary: Récupérer les données concernant un utilisateur
      description: |
        Commande permettant de récupérer un utilisateur à partir de son ID.

        #### Description des données renvoyées

        | Attribut          | Définition                              |
        |-------------------|-----------------------------------------|
        | **id**            | ID de l'utilisateur                     |
        | **username**      | Login de l'utilisateur                  |
        | **displayName**   | Nom afficher dans l'application         |
        | **email**         | Adresse mail de l'utilisateur           |
        | **state**         | Etat de l'utilisateur                   |
        | **lastRole**      | Dernier rôle utilisé                    |
        | **roles**         | Liste des roles de l'utilisateur        |
      tags:
        - User
      parameters:
        - name: user_id
          in: path
          description: ID de l'utilisateur
          required: true
          type: integer
      responses:
        200:
          description: Informations concernant un utilisateur
          schema:
            $ref: '#/definitions/User'
        400:
          description: Erreur. Requête mal formée
          schema:
            $ref: '#/definitions/Error'
        404:
          description: User introuvable
          schema:
            $ref: '#/definitions/Error'

    patch:
      summary: Modification d'un utilisateur
      description: |
        Commande permettant de modifier un utilisateur
        Il ne s'agit que d'un exemple d'utilisation, les modifications du username ou de l'id ne sont pas prise en compte
        
        #### Description des éléments json à envoyer

        | Attribut          | Définition                              |
        |-------------------|-----------------------------------------|
        | **id**            | ID de l'utilisateur                     |
        | **username**      | Login de l'utilisateur                  |
        | **displayName**   | Nom afficher dans l'application         |
        | **email**         | Adresse mail de l'utilisateur           |
        | **state**         | Etat de l'utilisateur                   |

      tags:
        - User
      consumes:
        - application/json
      parameters:
        - name: user_id
          in: path
          description: ID de l'utilisateur
          required: true
          type: integer
        - in: body
          name: data
          description: "json contenant les données à modifier de l'utilisateur
        "
          schema:
            type: object
            properties:
              id:
                type: integer
              username:
                type: string
              displayName:
                type: string
              email:
                type: string
              state:
                type: boolean
      responses:
        200:
          description: Informations concernant un utilisateur
          schema:
            $ref: '#/definitions/User'
        400:
          description: Erreur. Requête mal formée
          schema:
            $ref: '#/definitions/Error'
        404:
          description: User introuvable
          schema:
            $ref: '#/definitions/Error'

    delete:
      summary: Suppression d'un utilisateur
      description: |
        Commande permettant de supprimer 1 utilisateur. 
        Il ne s'agit que d'un exemple, les modifications en bdd sont désactivées
      parameters:
        - name: user_id
          in: path
          description: ID de l'utilisateur
          required: true
          type: integer
      tags:
        - User

      responses:
        200:
          description: Success
        400:
          description: Erreur. Requête mal formée
          schema:
            $ref: '#/definitions/Error'
        404:
          description: User introuvable
          schema:
            $ref: '#/definitions/Error'

  /api/Role:
    get:
      summary: Rechercher des roles
      description: |
        Commande permettant de rechercher des roles à partir de différents paramètres.

        #### Description des données renvoyées

        | Attribut          | Définition                            |
        |-------------------|---------------------------------------|
        | **id**            | ID du Rôle                            |
        | **code**          | Code du rôle                          |
        | **libelle**       | Libellé du rôle                       |
        | **description**   | Description du rôle                   |
        | **auto**          | Rôle automatique ou manuelle ?        |
        | **parent**        | Rôle parent                           |
        | **users**         | Liste des utilisateurs ayant le rôle  |
      tags:
        - Role
      parameters:
        - name: id
          in: query
          description: id du rôle
          type: integer
        - name: code
          in: query
          description:  Login Code du role
          type: string
        - name: auto
          in: query
          description: true = automatique, false = manuel
          type: boolean
        - $ref: '#/parameters/triFieldsParam'
      responses:
        200:
          description: Liste des roles
          schema:
            type: array
            items:
              $ref: '#/definitions/Role'
        400:
          description: Erreur. Requête mal formée
          schema:
            $ref: '#/definitions/Error'

  /api/role/{role_id}:
    get:
      summary: Récupérer les données concernant un rôle
      description: |
        Commande permettant de récupérer un rôle à partir de son ID.

        #### Description des données renvoyées

        | Attribut          | Définition                            |
        |-------------------|---------------------------------------|
        | **id**            | ID du Rôle                            |
        | **code**          | Code du rôle                          |
        | **libelle**       | Libellé du rôle                       |
        | **description**   | Description du rôle                   |
        | **auto**          | Rôle automatique ou manuelle ?        |
        | **parent**        | Rôle parent                           |
        | **users**         | Liste des utilisateurs ayant le rôle  |
      tags:
        - Role
      parameters:
        - name: role_id
          in: path
          description: ID du role
          required: true
          type: integer
      responses:
        200:
          description: Informations concernant un rôle
          schema:
            $ref: '#/definitions/Role'
        400:
          description: Erreur. Requête mal formée
          schema:
            $ref: '#/definitions/Error'
        404:
          description: Role introuvable
          schema:
            $ref: '#/definitions/Error'


definitions:
  Error:
    type: object
    properties:
      title:
        type: string
        description: Intitulé de l'erreur
      status:
        type: integer
        description: Code HTTP de l'erreur
      detail:
        type: string
        description: Explication de l'erreur

  Date:
    type: object
    properties:
      date:
        type: string
        pattern: "Y-m-d H:i:s"
      timezone_type:
        type: integer
      timezone:
        type: string

  User :
    type: object
    properties:
      id:
        type: integer
        description: ID de l'utilisateur
      username:
        type: string
        description: Loggin de l'utilisateur
      displayName:
        type: string
        description: Nom afficher dans l'application (Format par défaut - Nom Prénom)
      email:
        type: string
        description: adresse mail de l'utilisateur
      state:
        type: boolean
        pattern: '(1|0)'
        description: actif | désactivé/bloqué
      lastRole:
        $ref: '#/definitions/Role'
        description: Dernier rôle utilisé par l'utilisateur
      roles:
        type: array
        items:
          $ref: '#/definitions/Role'
        description: Liste des rôles de l'utilisateur
    required:
      - id
      - username
      - displayName
      - state

  UserLight :
    type: object
    properties:
      id:
        type: integer
        description: ID de l'utilisateur
      username:
        type: string
        description: Loggin de l'utilisateur
      displayName:
        type: string
        description: Nom afficher dans l'application (Format par défaut - Nom Prénom)
      email:
        type: string
        description: adresse mail de l'utilisateur
      state:
        type: boolean
        pattern: '(1|0)'
        description: actif | désactivé/bloqué
      lastRole:
        type: integer
        description: ID du dernier rôle utilisé
      roles:
        type: array
        items:
          type: integer
        description: Liste des identifiants des rôles de l'utilisateur
    required:
      - id
      - username
      - displayName
      - state

  Role :
    type: object
    properties:
      id:
        type: integer
        description: ID du rôle
      code:
        type: string
        description: Code du role
      libelle:
        type: string
        description: Libellé du rôle
      dscription:
        type: string
        description: Description du rôle
      auto:
        type: boolean
        description: Le rôle est-il attribué automatiquement
      parent:
        description: Rôle parent pour définir un héritage de rôle
        $ref: '#/definitions/RoleLight'
      users:
        type: array
        items:
          $ref: '#/definitions/UserLight'
        description: Liste des utilisateurs ayant ce rôle
    required:
      - id
      - code
      - libelle

  RoleLight:
    type: object
    properties:
      id:
        type: integer
        description: ID du rôle
      code:
        type: string
        description: Code du role
      libelle:
        type: string
        description: Libellé du rôle
      dscription:
        type: string
        description: Description du rôle
      auto:
        type: boolean
        description: Le rôle est-il attribué automatiquement
      parent:
        type: integer
        description: Id du Rôle parent pour définir un héritage de rôle
      users:
        type: array
        items:
          type: integer
        description: Liste des identifiants des utilisateurs ayant ce rôle
    required:
      - id
      - code
      - libelle
