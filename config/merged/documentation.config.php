<?php

namespace Api;

use Api\Controller\DocumentationController;
use Api\Controller\DocumentationControllerFactory;
use Api\Controller\RequestControllerFactory;
use Api\Controller\RequestController;
use Api\Form\ApiTestForm;
use Api\Form\Factory\ApiTestFormFactory;
use Api\Provider\Privilege\AdministrationPrivileges;
use UnicaenPrivilege\Guard\PrivilegeController;
use Laminas\ApiTools\Documentation as ApiToolsDocumentation;

return [
    'navigation' => [
        'default' => [
            'home' => [
                'pages' => [
                    'administration' => [
                        'pages' => [
                            [
                                'label' => 'API',
                                'route' => 'api/lister',
                                'resource' => 'controller/Laminas\\ApiTools\\Documentation\\Controller',
                                'dropdown-header' => true,
                                'icon' => 'fas fa-cogs',
                                'order' => 330,
                            ],
                            [
                                'label' => 'Gérer l\'API',
                                'title' => 'Interface d\'administration des API',
                                'route' => 'api-tools/ui',
                                'resource' => 'controller/Laminas\\ApiTools\\Admin\\Controller\\App',
                                'icon' => 'fas fa-angle-right',
                                'order' => 331,
                            ],
                            [
                                'label' => 'Documentation de l\'API',
                                'title' => 'Documentation de l\'API',
                                'route' => 'api/lister',
                                'resource' => 'controller/Laminas\\ApiTools\\Documentation\\Controller',
                                'icon' => 'fas fa-angle-right',
                                'order' => 332,
                            ],
                        ]
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'api' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/api',
                    'defaults' => [
                        'controller' => RequestController::class,
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'lister' => [
                        'type' => 'segment',
                        'options' => [
                            'route' => '/lister',
                            'defaults' => [
                                'controller' => 'Laminas\\ApiTools\\Documentation\\Controller',
                                'action' => 'show',
                            ],
                        ],
                    ],
                    'documentation' => [
                        'type' => 'segment',
                        'options' => [
                            'route' => '/documentation[/:api[-v:version][/:service]]',
                            'constraints' => [
                                'api' => '[a-zA-Z][a-zA-Z0-9_.%]+',
                                'version' => '[0-9]+',
                            ],
                            'defaults' => [
                                'controller' => DocumentationController::class,
                                'action' => 'afficher',
                            ],
                        ],
                    ],
                    'definition-yaml' => [
                        'type' => 'segment',
                        'options' => [
                            'route' => '/definition-yaml[/:api[-v:version]]',
                            'constraints' => [
                                'api' => '[a-zA-Z][a-zA-Z0-9_.%]+',
                            ],
                            'defaults' => [
                                'controller' => DocumentationController::class,
                                'action' => 'definition-yaml',
                            ],
                        ],
                    ],
                    'tester' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/tester',
                            'defaults' => [
                                'action' => 'tester',
                            ],
                        ],
                    ],
                ],
            ],

        ],
    ],

    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => RequestController::class,
                    'action' => [
                        'tester',
                    ],
                    'privileges' => [
                        AdministrationPrivileges::API_ADMINISTRATION,
                    ],
                ],
                [
                    'controller' => DocumentationController::class,
                    'action' => [
                        'afficher',
                        'definition-yaml',
                    ],
                    'privileges' => [
                        AdministrationPrivileges::API_DOCUMENTATION,
                    ],
                ],
                [
                    'controller' => [
                        // Api Documentation
                        ApiToolsDocumentation\Controller::class,
                    ],
                    'privileges' => [
                        AdministrationPrivileges::API_DOCUMENTATION,
                    ],
//                    'roles' => [],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'invokables' => [
        ],
        'factories' => [
        ],
    ],

    'controllers' => [
        'factories' => [
            RequestController::class => RequestControllerFactory::class,
            DocumentationController::class => DocumentationControllerFactory::class
        ],
    ],

    'form_elements' => [
        'factories' => [
            ApiTestForm::class => ApiTestFormFactory::class,
        ],
    ],

    'view_manager' => [
        'template_path_stack' => [
            'api-tools-documentation' => __DIR__ . '/../../view',
        ],
        'template_map' => [
            'api-tools-documentation/api-list' => __DIR__ . '/../../view/api/documentation/lister.phtml',
        ],
    ],
];