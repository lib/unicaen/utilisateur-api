<?php

namespace Api;

use Api\V1\Entity\Db\Role;
use Api\V1\Entity\Db\RoleLight;
use Api\V1\Entity\Db\User;
use Api\V1\Entity\Db\UserLight;
use Api\V1\Entity\Strategy\UniDirectionalToManyEntitiesStrategy;
use Api\V1\Entity\Strategy\UniDirectionalToManyIdsStrategy;
use Api\V1\Entity\Strategy\UniDirectionalToOneIdStrategy;
use Api\V1\Rest\Role\RoleCollection;
use Api\V1\Rest\Role\RoleLightCollection;
use Api\V1\Rest\Role\RoleLightResource;
use Api\V1\Rest\Role\RoleResource;
use Api\V1\Rest\User\Hydrator\UserHydrator;
use Api\V1\Rest\User\Hydrator\UserHydratorFactory;
use Api\V1\Rest\User\UserCollection;
use Api\V1\Rest\User\UserLightCollection;
use Api\V1\Rest\User\UserLightResource;
use Api\V1\Rest\User\UserResource;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\Filter\ToInt;
use Laminas\I18n\Validator\IsInt;
use Laminas\Validator\StringLength;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'router' => [
        'routes' => [
            'api.rest.doctrine.user' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/api[/v:version]/user[/:user_id]',
                    'constraints' => [
                        'user_id' => '\\d+',
                    ],
                    'defaults' => [
                        'controller' => 'Api\\V1\\Rest\\User\\Controller',
                    ],
                ],
            ],
            'api.rest.doctrine.user-light' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/api[/v:version]/user-light[/:user_id]',
                    'constraints' => [
                        'user_id' => '\\d+',
                    ],
                    'defaults' => [
                        'controller' => 'Api\\V1\\Rest\\UserLight\\Controller',
                    ],
                ],
            ],
            'api.rest.doctrine.role' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/api[/v:version]/role[/:role_id]',
                    'constraints' => [
                        'role_id' => '\\d+',
                    ],
                    'defaults' => [
                        'controller' => 'Api\\V1\\Rest\\Role\\Controller',
                    ],
                ],
            ],
            'api.rest.doctrine.role-light' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/api[/v:version]/role-light[/:role_id]',
                    'constraints' => [
                        'role_id' => '\\d+',
                    ],
                    'defaults' => [
                        'controller' => 'Api\\V1\\Rest\\RoleLight\\Controller',
                    ],
                ],
            ],
        ],
    ],

    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => [
                        'Api\\V1\\Rest\\User\\Controller',
                        'Api\\V1\\Rest\\UserLight\\Controller',
                        'Api\\V1\\Rest\\Role\\Controller',
                        'Api\\V1\\Rest\\RoleLight\\Controller',
                    ],
                    'roles' => [],
                ],
            ],
        ],
    ],

    'api-tools-versioning' => [
        'uri' => [
            'api.rest.doctrine.user',
            'api.rest.doctrine.user-light',
            'api.rest.doctrine.role',
            'api.rest.doctrine.role-light',
        ],
        'default_version' => 1,
    ],

    'api-tools-rest' => [
        'Api\\V1\\Rest\\User\\Controller' => [
            'listener' => UserResource::class,
            'service_name' => 'User',
            'route_name' => 'api.rest.doctrine.user',
            'route_identifier_name' => 'user_id',
            'entity_class' => User::class,
            'entity_identifier_name' => 'id',
            'entity_http_methods' => ['GET', 'POST', 'PATCH', 'DELETE'],
            'collection_class' => UserCollection::class,
            'collection_name' => 'user',
            'collection_http_methods' => ['GET', 'POST', 'PATCH', 'DELETE'], // Méthode d'appel
            'collection_query_whitelist' => ['username', 'email', 'state', 'order-by'], //liste des paramétres que l'on autorise dans la requêtes
            'page_size' => '10', //-1 = tout les résultat, sinon nombre de résultat par page de requêtes
        ],
        'Api\\V1\\Rest\\UserLight\\Controller' => [
            'listener' => UserLightResource::class,
            'service_name' => 'UserLight',
            'route_name' => 'api.rest.doctrine.user-light',
            'route_identifier_name' => 'user_id',
            'entity_class' => UserLight::class,
            'entity_identifier_name' => 'id',
            'entity_http_methods' => ['GET'],
            'collection_class' => UserLightCollection::class,
            'collection_name' => 'user',
            'collection_http_methods' => ['GET'], // Méthode d'appel
            'collection_query_whitelist' => ['username', 'email', 'state', 'order-by'], //liste des paramétres que l'on autorise dans la requêtes
            'page_size' => '10', //-1 = tout les résultat, sinon nombre de résultat par page de requêtes
        ],
        'Api\\V1\\Rest\\Role\\Controller' => [
            'listener' => RoleResource::class,
            'service_name' => 'Role',
            'route_name' => 'api.rest.doctrine.role',
            'route_identifier_name' => 'role_id',
            'entity_class' => Role::class,
            'entity_identifier_name' => 'id',
            'entity_http_methods' => ['GET'],
            'collection_class' => RoleCollection::class,
            'collection_name' => 'role',
            'collection_http_methods' => ['GET'],
            'collection_query_whitelist' => ['code', 'libelle', 'order-by'], //liste des paramétres que l'on autorise dans la requêtes
            'page_size' => '-1',
        ],
        'Api\\V1\\Rest\\RoleLight\\Controller' => [
            'listener' => RoleLightResource::class,
            'service_name' => 'RoleLight',
            'route_name' => 'api.rest.doctrine.role-light',
            'route_identifier_name' => 'role_id',
            'entity_class' => RoleLight::class,
            'entity_identifier_name' => 'id',
            'entity_http_methods' => ['GET'],
            'collection_class' => RoleLightCollection::class,
            'collection_name' => 'role',
            'collection_http_methods' => ['GET'],
            'collection_query_whitelist' => ['code', 'libelle', 'order-by'], //liste des paramétres que l'on autorise dans la requêtes
            'page_size' => '-1',
        ],
    ],

    'api-tools-content-negotiation' => [
        'controllers' => [
            'Api\\V1\\Rest\\User\\Controller' => 'HalJson',
            'Api\\V1\\Rest\\UserLight\\Controller' => 'HalJson',
            'Api\\V1\\Rest\\Role\\Controller' => 'HalJson',
            'Api\\V1\\Rest\\RoleLight\\Controller' => 'HalJson',
        ],
        'accept_whitelist' => [
            'Api\\V1\\Rest\\User\\Controller' => [
                0 => 'application/vnd.api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
            'Api\\V1\\Rest\\UserLight\\Controller' => [
                0 => 'application/vnd.api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
            'Api\\V1\\Rest\\Role\\Controller' => [
                0 => 'application/vnd.api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
            'Api\\V1\\Rest\\RoleLight\\Controller' => [
                0 => 'application/vnd.api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
        ],
        'content_type_whitelist' => [
            'Api\\V1\\Rest\\User\\Controller' => [
                0 => 'application/vnd.api.v1+json',
                1 => 'application/json',
            ],
            'Api\\V1\\Rest\\UserLight\\Controller' => [
                0 => 'application/vnd.api.v1+json',
                1 => 'application/json',
            ],
            'Api\\V1\\Rest\\Role\\Controller' => [
                0 => 'application/vnd.api.v1+json',
                1 => 'application/json',
            ],
            'Api\\V1\\Rest\\RoleLight\\Controller' => [
                0 => 'application/vnd.api.v1+json',
                1 => 'application/json',
            ],
        ],
    ],

    'api-tools-hal' => [
        'metadata_map' => [
            User::class => [
                'route_identifier_name' => 'user_id',
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.doctrine.user',
                'hydrator' => 'Api\\V1\\Rest\\User\\Hydrator\\UserHydrator',
            ],
            UserCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.doctrine.user',
                'is_collection' => true,
            ],
            UserLight::class => [
                'route_identifier_name' => 'user_id',
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.doctrine.user-light',
                'hydrator' => 'Api\\V1\\Rest\\UserLight\\UserLightHydrator',
            ],
            UserLightCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.doctrine.user-light',
                'is_collection' => true,
            ],
            Role::class => [
                'route_identifier_name' => 'role_id',
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.doctrine.role',
                'hydrator' => 'Api\\V1\\Rest\\Role\\RoleHydrator',
            ],
            RoleCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.doctrine.role',
                'is_collection' => true,
            ],
            RoleLight::class => [
                'route_identifier_name' => 'role_id',
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.doctrine.role-light',
                'hydrator' => 'Api\\V1\\Rest\\RoleLight\\RoleLightHydrator',
            ],
            RoleLightCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'api.rest.doctrine.role-light',
                'is_collection' => true,
            ],
        ],
    ],

    'api-tools' => [
        'doctrine-connected' => [
            UserResource::class => [
                'object_manager' => 'doctrine.entitymanager.orm_default',
                'hydrator' => 'Api\\V1\\Rest\\User\\Hydrator\\UserHydrator',
                'query_providers' => [
                    'default' => 'default_orm',
                    'fetch_all' => 'user_fetch_all',
                ],
            ],
            UserLightResource::class => [
                'object_manager' => 'doctrine.entitymanager.orm_default',
                'hydrator' => 'Api\\V1\\Rest\\UserLight\\UserLightHydrator',
                'query_providers' => [
                    'default' => 'default_orm',
                    'fetch_all' => 'user_fetch_all',
                ],
            ],
            RoleResource::class => [
                'object_manager' => 'doctrine.entitymanager.orm_default',
                'hydrator' => 'Api\\V1\\Rest\\Role\\RoleHydrator',
                'query_providers' => [
                    'default' => 'default_orm',
                    'fetch_all' => 'role_fetch_all',
                ],
            ],
            RoleLightResource::class => [
                'object_manager' => 'doctrine.entitymanager.orm_default',
                'hydrator' => 'Api\\V1\\Rest\\RoleLight\\RoleLightHydrator',
                'query_providers' => [
                    'default' => 'default_orm',
                    'fetch_all' => 'role_fetch_all',
                ],
            ],
        ],
    ],

    'doctrine-hydrator' => [
        'Api\\V1\\Rest\\User\\Hydrator\\UserHydrator' => [ // on a changer ici l'hydrator
            'entity_class' => User::class,
            'object_manager' => 'doctrine.entitymanager.orm_default',
            'by_value' => true,
            'strategies' => [
                'roles' => UniDirectionalToManyEntitiesStrategy::class,
            ],
//            'use_generated_hydrator' => true,
            'use_generated_hydrator' => false,
        ],
        'Api\\V1\\Rest\\UserLight\\UserLightHydrator' => [
            'entity_class' => UserLight::class,
            'object_manager' => 'doctrine.entitymanager.orm_default',
            'by_value' => true,
            'strategies' => [
                'roles' => UniDirectionalToManyIdsStrategy::class,
            ],
            'use_generated_hydrator' => true,
        ],
        'Api\\V1\\Rest\\Role\\RoleHydrator' => [
            'entity_class' => Role::class,
            'object_manager' => 'doctrine.entitymanager.orm_default',
            'by_value' => true,
            'strategies' => [
                'users' => UniDirectionalToManyEntitiesStrategy::class,
            ],
            'use_generated_hydrator' => true,
        ],
        'Api\\V1\\Rest\\RoleLight\\RoleLightHydrator' => [
            'entity_class' => RoleLight::class,
            'object_manager' => 'doctrine.entitymanager.orm_default',
            'by_value' => true,
            'strategies' => [
                'users' => UniDirectionalToManyIdsStrategy::class,
                'parent' => UniDirectionalToOneIdStrategy::class,
            ],
            'use_generated_hydrator' => true,
        ],
    ],


    'api-tools-content-validation' => [
        'Api\\V1\\Rest\\User\\Controller' => [
            'input_filter' => 'Api\\V1\\Rest\\User\\Validator',
        ],
        'Api\\V1\\Rest\\UserLight\\Controller' => [
            'input_filter' => 'Api\\V1\\Rest\\UserLight\\Validator',
        ],
        'Api\\V1\\Rest\\Role\\Controller' => [
            'input_filter' => 'Api\\V1\\Rest\\Role\\Validator',
        ],
        'Api\\V1\\Rest\\RoleLight\\Controller' => [
            'input_filter' => 'Api\\V1\\Rest\\RoleLight\\Validator',
        ],
    ],

    'input_filter_specs' => [
        'Api\\V1\\Rest\\User\\Validator' => [
            'id' => [
                'name' => 'id',
                'required' => false,
                'filters' => [
                    [ 'name' => ToInt::class, ],
                ],
                'validators' => [
                    [
                        'name' => IsInt::class,
                    ],
                ],
            ],
            'username' => [
                'name' => 'username',
                'required' => false,
                'filters' => [
                    [ 'name' => StringTrim::class, ],
                    [ 'name' => StripTags::class, ],
                ],
                'validators' => [
                    [
                        'name' => StringLength::class,
                        'options' => [
                            'min' => 1,
                            'max' => 255,
                        ],
                    ],
                ],
            ],
            'displayName' => [
                'name' => 'displayName',
                'required' => false,
                'filters' => [
                    [ 'name' => StringTrim::class, ],
                    [ 'name' => StripTags::class, ],
                ],
                'validators' => [
                    [
                        'name' => StringLength::class,
                        'options' => [
                            'min' => 1,
                            'max' => 255,
                        ],
                    ],
                ],
            ],
            'email' => [
                'name' => 'email',
                'required' => false,
                'filters' => [
                    [ 'name' => StringTrim::class, ],
                    [ 'name' => StripTags::class, ],
                ],
                'validators' => [
                    [
                        'name' => StringLength::class,
                        'options' => [
                            'min' => 1,
                            'max' => 255,
                        ],
                    ],
                ],
            ],
        ],
//        'Api\\V1\\Rest\\Role\\Validator' => [
//            0 => [
//                'name' => 'code',
//                'required' => false,
//                'filters' => [
//                    0 => [
//                        'name' => 'Laminas\\Filter\\StringTrim',
//                    ],
//                    1 => [
//                        'name' => 'Laminas\\Filter\\StripTags',
//                    ],
//                    2 => [
//                        'name' => 'Laminas\\Validator\\StringLength',
//                        'options' => [
//                                'min' => 1,
//                                'max' => 64,
//                        ],
//                    ],
//                ],
//            ],
//            1 => [
//                'name' => 'libelle',
//                'required' => false,
//                'filters' => [
//                    0 => [
//                        'name' => 'Laminas\\Filter\\StringTrim',
//                    ],
//                    1 => [
//                        'name' => 'Laminas\\Filter\\StripTags',
//                    ],
//                    2 => [
//                        'name' => 'Laminas\\Validator\\StringLength',
//                        'options' => [
//                                'min' => 1,
//                                'max' => 255,
//                        ],
//                    ],
//                ],
//            ],
//            2 => [
//                'name' => 'description',
//                'required' => false,
//                'filters' => [
//                    0 => [
//                        'name' => 'Laminas\\Filter\\StringTrim',
//                    ],
//                    1 => [
//                        'name' => 'Laminas\\Filter\\StripTags',
//                    ],
//                ],
//            ],
//        ]
//        Pour l'exemple
//        'Api\\V1\\Rest\\CarriereBap\\Validator' => [
//            0 => [
//                'name' => 'code',
//                'required' => true,
//                'filters' => [
//                    0 => [
//                        'name' => 'Laminas\\Filter\\StringTrim',
//                    ],
//                    1 => [
//                        'name' => 'Laminas\\Filter\\StripTags',
//                    ],
//                ],
//                'validators' => [
//                    0 => [
//                        'name' => 'Laminas\\Validator\\StringLength',
//                        'options' => [
//                            'min' => 1,
//                            'max' => 10,
//                        ],
//                    ],
//                ],
//            ],
//            1 => [
//                'name' => 'libelleCourt',
//                'required' => true,
//                'filters' => [
//                    0 => [
//                        'name' => 'Laminas\\Filter\\StringTrim',
//                    ],
//                    1 => [
//                        'name' => 'Laminas\\Filter\\StripTags',
//                    ],
//                ],
//                'validators' => [
//                    0 => [
//                        'name' => 'Laminas\\Validator\\StringLength',
//                        'options' => [
//                            'min' => 1,
//                            'max' => 20,
//                        ],
//                    ],
//                ],
//            ],
//        ],
    ],

    'api-tools-mvc-auth' => [
        'authorization' => [
            'Api\\V1\\Rest\\User\\Controller' => [
                'collection' => [
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => true,
                    'DELETE' => false,
                ],
                'entity' => [
                    'GET' => true,
                    'POST' => true,
                    'PUT' => false,
                    'PATCH' => true,
                    'DELETE' => true,
                ],
            ],
            'Api\\V1\\Rest\\UserLight\\Controller' => [
                'collection' => [
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ],
                'entity' => [
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ],
            ],
            'Api\\V1\\Rest\\Role\\Controller' => [
                'collection' => [
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ],
                'entity' => [
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ],
            ],
            'Api\\V1\\Rest\\RoleLight\\Controller' => [
                'collection' => [
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ],
                'entity' => [
                    'GET' => true,
                    'POST' => false,
                    'PUT' => false,
                    'PATCH' => false,
                    'DELETE' => false,
                ],
            ],
        ],
    ],

    /** Déclaration des stategies */
    'service_manager' => [
        'invokables' => [
            UniDirectionalToManyEntitiesStrategy::class => UniDirectionalToManyEntitiesStrategy::class,
            UniDirectionalToManyIdsStrategy::class => UniDirectionalToManyIdsStrategy::class,
            UniDirectionalToOneIdStrategy::class => UniDirectionalToOneIdStrategy::class,
        ],
    ],
    /** Hydrator */
    'hydrators' => [
        'factories' => [
            UserHydrator::class => UserHydratorFactory::class,
        ]
    ],
];