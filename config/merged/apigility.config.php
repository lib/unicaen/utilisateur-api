<?php

namespace Api;

use Api\Provider\Privilege\AdministrationPrivileges;
use Laminas\ApiTools\Admin as ApiToolsAdmin;
use Laminas\ApiTools\Doctrine\Admin as ApiToolsDoctrineAdmin;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    // Ajout d'ACL pour accéder à Apigility
                    'controller' => [
                        // Apigility
                        ApiToolsAdmin\Controller\ApiToolsVersionController::class,
                        ApiToolsAdmin\Controller\App::class,
                        ApiToolsAdmin\Controller\Authentication::class,
                        ApiToolsAdmin\Controller\AuthenticationType::class,
                        ApiToolsAdmin\Controller\Authorization::class,
                        ApiToolsAdmin\Controller\CacheEnabled::class,
                        ApiToolsAdmin\Controller\Config::class,
                        ApiToolsAdmin\Controller\ContentNegotiation::class,
                        ApiToolsAdmin\Controller\Dashboard::class,
                        ApiToolsAdmin\Controller\DbAdapter::class,
                        ApiToolsAdmin\Controller\DbAutodiscovery::class,
                        ApiToolsAdmin\Controller\DoctrineAdapter::class,
                        ApiToolsAdmin\Controller\Documentation::class,
                        ApiToolsAdmin\Controller\Filters::class,
                        ApiToolsAdmin\Controller\FsPermissions::class,
                        ApiToolsAdmin\Controller\HttpBasicAuthentication::class,
                        ApiToolsAdmin\Controller\HttpDigestAuthentication::class,
                        ApiToolsAdmin\Controller\Hydrators::class,
                        ApiToolsAdmin\Controller\InputFilter::class,
                        ApiToolsAdmin\Controller\Module::class,
                        ApiToolsAdmin\Controller\ModuleConfig::class,
                        ApiToolsAdmin\Controller\ModuleCreation::class,
                        ApiToolsAdmin\Controller\OAuth2Authentication::class,
                        ApiToolsAdmin\Controller\Package::class,
                        ApiToolsAdmin\Controller\RestService::class,
                        ApiToolsAdmin\Controller\RpcService::class,
                        ApiToolsAdmin\Controller\SettingsDashboard::class,
                        ApiToolsAdmin\Controller\Source::class,
                        ApiToolsAdmin\Controller\Strategy::class,
                        ApiToolsAdmin\Controller\Validators::class,
                        ApiToolsAdmin\Controller\Versioning::class,

                        // Apigility Doctrine
                        ApiToolsDoctrineAdmin\Controller\DoctrineAutodiscovery::class,
                        ApiToolsDoctrineAdmin\Controller\DoctrineRestService::class,
                        ApiToolsDoctrineAdmin\Controller\DoctrineRpcService::class,
                        ApiToolsDoctrineAdmin\Controller\DoctrineMetadataService::class,
                    ],
                    'privileges' => [
                        AdministrationPrivileges::API_ADMINISTRATION,
                    ],
                ],
            ],
        ],
    ],
];