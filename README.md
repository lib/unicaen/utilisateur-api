# API-Tool

Exemple d'utilisation de API-Tool pour accéder à UnicaenUtilisateur

Documentation : 
https://api-tools.getlaminas.org/documentation/intro/getting-started

## Instalisation

### Configuration

Rajouter dans `config/application.config.php` :

``` 
    //Doivent être définie avant :
    'DoctrineModule',
    'DoctrineORMModule',
    'ZfcUser',
    'BjyAuthorize',
    
    //...
    
    // API
    'Laminas\\ApiTools',
    'ApiSkeletons\\DoctrineORMHydrationModule',
    'Laminas\\ApiTools\\Doctrine\\Server',
    'Laminas\\ApiTools\\Doctrine\\QueryBuilder',
    'Laminas\\ApiTools\\Documentation',
    'Laminas\\ApiTools\\Documentation\\Swagger',
    'Laminas\\ApiTools\\ApiProblem',
    'Laminas\\ApiTools\\Configuration',
    'Laminas\\ApiTools\\MvcAuth',
    'Laminas\\ApiTools\\OAuth2',
    'Laminas\\ApiTools\\Hal',
    'Laminas\\ApiTools\\ContentNegotiation',
    'Laminas\\ApiTools\\ContentValidation',
    'Laminas\\ApiTools\\Rest',
    'Laminas\\ApiTools\\Rpc',
    'Laminas\\ApiTools\\Versioning',
    'Laminas\\DeveloperTools',
    'Laminas\\ApiTools\Admin',
    'Laminas\\ApiTools\Doctrine\Admin',    
    'Api',
```

Rajouter dans `composer.json` les entrées suivante :

``` 
"require": {
    "laminas-api-tools/api-tools": "^1.3",
    "laminas-api-tools/api-tools-documentation": "^1.2-dev",
    "laminas-api-tools/api-tools-documentation-swagger": "^1.2",
    "api-skeletons/doctrine-orm-hydration-module": "dev-master",

},

"require-dev": {  
    "laminas-api-tools/api-tools-admin": "~1.0",
    "laminas-api-tools/api-tools-asset-manager": "^1.0",
},

"scripts": {
    "pre-install-cmd": [
      "[ -d 'vendor/laminas-api-tools/api-tools-doctrine' ] || git clone https://github.com/orderadmin/api-tools-doctrine vendor/laminas-api-tools/api-tools-doctrine",
      "[ -d 'vendor/laminas-api-tools/api-tools-doctrine-querybuilder' ] || git clone https://github.com/orderadmin/api-tools-doctrine-querybuilder vendor/laminas-api-tools/api-tools-doctrine-querybuilder"
    ],
    "pre-update-cmd": [
      "[ -d 'vendor/laminas-api-tools/api-tools-doctrine' ] || git clone https://github.com/orderadmin/api-tools-doctrine vendor/laminas-api-tools/api-tools-doctrine",
      "[ -d 'vendor/laminas-api-tools/api-tools-doctrine-querybuilder' ] || git clone https://github.com/orderadmin/api-tools-doctrine-querybuilder vendor/laminas-api-tools/api-tools-doctrine-querybuilder"
    ],
    "post-install-cmd": [    
        "cp -r vendor/unicaen/exemple/config/api.global.php.dist   config/autoload/api.global.php",
        "cp -r vendor/unicaen/exemple/config/api.local.php.dist   config/autoload/api.local.php",
    ]
},
"autoload": {
    "psr-4": {    
        "Api\\": "module/Api/src",        
        "Laminas\\ApiTools\\Doctrine\\": "vendor/laminas-api-tools/api-tools-doctrine/src",
        "Laminas\\ApiTools\\Doctrine\\QueryBuilder\\": "vendor/laminas-api-tools/api-tools-doctrine-querybuilder/src"
    },
},
"config": {
    "allow-plugins": {
        "laminas-api-tools/api-tools-asset-manager": true
    }
}
```

Les commandes de pre-install et pre-update permette de faire le git clone et de gérer les problème d'incompatibilité des librairies laminas-api-tools/api-tools-doctine*
- Choix fait de ne pas faire de pull si le fichier existe 
- Le fait de le faire dans le pre-update est discutable, Celà est un équivalent du update des librairie vendor, ca semble logique de le faire ici.


### htpasswd

Se placer dans le répertoire `config` de l'application et lancez la
commande suivante pour créer le fichier `users.htpasswd` contenant un utilisateur par défaut `AppName` avec son mot de passe
```bash
htpasswd -cs users.htpasswd AppName
```

<b>Attention :</b> faire le fichier `config/.gitignore` contenant :
``` 
    *.htpasswd
```

### BDD

2 priviléges nécessaires : api-administration et api-documentation
Cf. `SQL/001_privileges.sql`


## Creer un Webservice 

Pour les exemples ici, on utilisera User et role de UnicaenUtilisateur pour avoir une entité commune aux applications.
On se place ici dans l'hypothèse d'une premiére version de l'API : V1


### Entity/Db

Liens entre la BDD et les entités gérer par l'API. Proche de la version classique de doctrine avec le Mapping ...
- Permet de faire quelques distinction avec l'entité manipuler dans l'application et celle dans l'API ie. ici on ne prend pas les champs password et password_rest_token
- On peut également faire une des version lights  de classes, nottaments pour gerer les relations n-ére et éviter les boucles. Ces versions lights utilise nottaments des stategies pour hydrater les collections avec les ID et non les entités elle-même.
Dans notre cas, User fait références a Rôle tandis que Role fait références a RoleLight pour son parent et à UserLight pour les Users.

### Hydrator
Par défaut, chaque entité aura un hydrator "fictif" `Api\\V1\\Rest\\User\\UserHydrator'`
Cette hydrator n'as pas a être instantié, ApiTools a un mécanisme d'hydratation, interne. Il s'agira ici d'une clé de configuration.
Il est important de bien définir leurs usages dans la configuration afin d'hydrater correctement les entités.
- `['api-tools-hal']['metadata_map']`
- `['api-tools-hal']['doctrine-connected']`
- `['doctrine-hydrator']`

On peut cependant redéfinir l'hydrator, ce qui est fait pour l'exemple avec `Api\V1\Rest\User\Hydrator\UserHydrator`

### Strategy
Pour completer les hydrators, on peut définir les stategy. On en a 3 déjà faites qui permettent de gerer les relations 1-n/n-n entre les entités dans les collections sans faire de boucles infini.
- `UniDirectionalToManyEntitiesStrategy`
- `UniDirectionalToManyIdsStrategy`
- `UniDirectionalToOneIdStrategy`

Les stratégies `RelationIdStrategy` permettent d'hydrater les collections en fournissant l'identifiant de l'entités et non l'entité elle même.

Dans notre exemple, on s'ens sert pour gerer la relation n-n entre Role et Utilisateur.

Cf la configuration `['doctrine-hydrator']['Api\\V1\\Rest\\User\\UserHydrator']`

### Service Rest

Pour chaque entité faire les classes suivantes requise par ApiTools pour gérer respectivement les ensembles et 1 instance de l'Entité :
- EntityNameCollection :
``` 
<?php
namespace Api\V1\Rest\User;

use Laminas\Paginator\Paginator;

class UserCollection extends Paginator{}
```

- UserRessource : 
``` 
<?php
namespace Api\V1\Rest\User;

use Laminas\ApiTools\Doctrine\Server\Resource\DoctrineResource;

class UserResource extends DoctrineResource
{
}
```

Faire le services FetchAll (et sa factory) qui détermine la requête de récupération des données :

``` 
<?php

namespace Api\V1\Rest\User;

use Api\V1\Query\Provider\AbstractQueryProvider;
use Laminas\ApiTools\Rest\ResourceEvent;

class UserFetchAll extends AbstractQueryProvider
{
    /**
     * @param ResourceEvent $event
     * @param string $entityClass
     * @param array $parameters
     * @return mixed|null
     */
    public function createQuery(ResourceEvent $event, $entityClass, $parameters)
    {
        parent::createQuery($event, $entityClass, $parameters);

        return parent::makeQuery($event, $entityClass, $parameters, $filter ?? []);
    }
}
``` 
A noté que l'on hérite ici de AbstractQueryProvider qui rajoute automatiquement 2 parametres aux filtres de requête : 
- order-by : parametre de trie de la requêtes. 
- page : parametre de pagination pour limiter le nombre de réponses

Si l'on souhaite rajouter d'autres paramétres de filtres / faire des jointures dans la requêtes ..., c'est ici que ça se passe

### Déclaration de l'API dans la Configuration
Le gros du boulots est ici. Dans `api.config.php`.
Pour chaque clé, je ne donne que les informations importantes, les 2 exemples dans le fichiers devrais suffire a comprendre.

- 'Router' : déclaration des routes d'appel à l'api
    - <b>Le controller</b> `Api\\V1\\Rest\\User\\Controller` est fictif et sert surtout de clé dans le reste de la configuration.
 
  - 'bjyauthorize' : pour éviter une erreur 403, 
  - l'authentification se faisant le htpasswd, on doit autorisé toutes les routes

- 'api-tools-versioning' : Déclaration des versions accessibles pour chaque service 
  - todo : voir comment déclarer les différentes versions d'un service car pour le momment on est rester sur la V1

- 'api-tools-rest' : paramétres de configuration de chaque service Rest
    - 'collection_query_whitelist' déclare l'ensemble des paramétres Post/Get qui peuvent être fournis lors d'appel en fetchALl.
    - 'page_size' : nombre de réponse renvoyer. -1 = tous les résultat 

- 'api-tools-content-negotiation' : déclaration du format des réponse

- 'api-tools-hal' : mapping entre les entités et le format HalJson ? (a vérifier)
  - !!! pour les objets, ne pas oublier de déclarer l'hydrator fictif, `Api\\V1\\Rest\\User\\UserHydrator`. C'est nottament lui qui gére les relations entre les objets.
  Si vous avez une erreur du type `Unable to determine entity identifier for object ...` c'est probablement l'erreur
- 'api-tools' : déclaration pour chaque entités des services et de l'ORM à appelé pour récuper les données de la bdd
- 'doctrine-hydrator' : déclaration de l'hydrator 
  - la clé 'use_generated_hydrator' => true  permet de générer l'hydrateur fictif Api\\V1\\Rest\\User\\UserHydrator
- 'api-tools-content-validation' : déclaration d'un validateur sur les entités
  - Todo A vérifier, a priorie marche comme pour le validateur d'un formulaire

- 'input_filter_specs' : Spécification du filter sur les valeurs de certains champs de l'entité
  -  Todo : a vériifier, a priori marche comme pour les InputFilter des formulaires

- 'api-tools-mvc-auth' : configuration des méthodes d'appels authorisé de l'API Rest

<b>Dans le fichier `api-tools-doctrine-querybuilder.config.php`</b>
- 'api-tools-doctrine-query-provider' : déclaration du service pour les fetchAll

## Pour l'ajout des entité
Pour permettre la modification d'une entité, il suffit d'autoriser l'appel en 'POST' de l'url.
Par exemple les 2 clés de configuration suivante autorise l'ajout d'un utilisateur :
``` 
['api-tools-rest']['Api\\V1\\Rest\\User\\Controller']['entity_http_methods'] = ['POST']
['api-tools-mvc-auth']['authorization']['Api\\V1\\Rest\\User\\Controller']['entity']['POST'] = true
``` 

- Pour chaque champs modifiables que l'on peut fournir dans le JSON, il faut penser à définir la spécification du filtre pour le validator.
``` 
  'input_filter_specs' => [
    'Api\\V1\\Rest\\User\\Validator' => [
        0 => [
          'name' => 'displayName',
          'required' => true,
          'filters' => [
            0 => [
              'name' => 'Laminas\\Filter\\StringTrim',
            ],
            1 => [
              'name' => 'Laminas\\Filter\\StripTags',
            ],
            2 => [
            'name' => 'Laminas\\Validator\\StringLength',
            'options' => [
                  'min' => 1,
                  'max' => 255,
            ],
          ],
        ],
      ],
    ...
    ],
  ],
``` 

- Les données doivent être fournis aux format json. Pour celà rajouter dans la commande curl les 3 paramétres suivant :
``` 
 -H 'Content-Type: application/json'
 -H 'Accept: application/json'
 -d '{Corps du JSON}'
``` 

Si vous avez une erreur du type "Unrecognized field \"nomDuChamps\"" c'est que la spécification du filter n'as pas été fait

- L'ajout de l'entité est alors gérer par la méthode `create` de `UserResource` (hérité de `DoctrineResource`).
- On a surchargé la méthode ici afin de garder l'exemple sans réelement permettre d'ajout d'un utilisateur

- <b>DoctrineAPITools ne permet l'ajout que d'une entité à la fois.</b>
Une solution de contournement du problème est proposé en utilisant la surcharge de `DoctrineResource` fournis dans `Api\V1\Doctrine`.
L'idée est de fournir une méthode createList dans le même principe que les patchList/deleteList existante. 
Il ne s'agit que d'une solution de contournement imparfaite qui détermine si le Json fournis est un tableau ou une seul entité.
Pour l'exemple ici, on l'utilise pour les UserResources.



## Pour la modification des entité
Pour permettre la modification d'une entité, il suffit d'autoriser l'appel en 'PATCH' de l'url.

Par exemple les 2 clés de configuration suivante autorise la suppression 1 à 1 des utilisateur :
``` 
['api-tools-rest']['Api\\V1\\Rest\\User\\Controller']['entity_http_methods'] = [..., 'PATCH']
['api-tools-mvc-auth']['authorization']['Api\\V1\\Rest\\User\\Controller']['entity']['PATCH'] = true
``` 
- Comme pour l'ajout, il faut avoir défini les inputFilter des champs que l'on souhaite pouvoir modifier
- L'appel se fait de la même maniére que l'ajout mais avec le protocol PATCH.
- La méthode de `DoctrineResource` a éventuellement surcharger est `patch()`

L'ajout de multiple données se passe de la même maniére, en fournissant le json sous forme d'un tableau contenant chaque entité à modifier
- Important : pour chaque donnée, il faut fournir le champs correspondant à la clé primaire afin d'identifier les données à modifier (classiquement id)
- La méthode `patchList()` de `DoctrineResource` peut éventuellement être surchargé au besoin, sachant que elle même par défaut fait appelle à `patch()` pour chaque entité.


## Pour la suppression des entités
Pour permettre la suppression d'une entité, il suffit d'autoriser l'appel du protocole `DELETE` de la même maniére que pour `POST` et `PATCH`.

- La méthode de `DoctrineResource` a surchargé est `delete`.
- Comme pour l'ajout on a surchargé la méthode ici afin de garder l'exemple sans réelement permettre la suppresion d'un utilisateur

La suppression multiple marche de la même maniére avec en entrée un tableaux JSON, devant contenir pour chaque entité l'identifiant.
- A noté que si l'une des suppression echoue, toutes sont annulé
- La méthode `deleteList()` de `DoctrineResource` peut éventuellement être surchargé au besoin, sachant que elle même par défaut fait appelle à `delete()` pour chaque entité.


## Documentation

### Personnalisation
DocumentationController est une surcouche local pour personnaliser éventuellement l'interface et l'intégrer dans l'application.
Le fichier `swagger-unistrap.scss` permet de spécifier a minima quelques paramétres.


### Contenue
Le contenue de la documentation est difinie en swager (version 2.0) dans le fichier `api-v1.yml`

https://swagger.io/docs/specification/2.0/basic-structure/

Penser à la faire pour chaque service afin de pouvoir y acceder depuis le menu d'administration


## Utilisation

### Depuis l'interface

Via l'url https://localhost:8443/api/documentation/Api-v1
- donne la doc et permet de la tester avec les paramétres ...
- nécessite de s'authentifier réguliérement (login/password du .hthaccess). </b>Attention, l'authentification ne dit pas si elle a échoué. On s'en rend compte qu'en recevant l'erreur 403</b>
- Conseil : utiliser un module de navigateur tel que RESTClient (Firefox)


### En curl :
- fetchAll :

``` 
curl --insecure GET -H 'Accept: application/hal+json' -H 'Authorization: Basic [TOKEN]' 'https://localhost:8443/api/user'
``` 

- fetchAll + page :

``` 
curl --insecure GET -H 'Accept: application/hal+json' -H 'Authorization: Basic [TOKEN]' 'https://localhost:8443/api/user?page=3'
``` 

- fetchAll + order :

``` 
curl --insecure GET -H 'Accept: application/hal+json' -H 'Authorization: Basic [TOKEN]' 'https://localhost:8443/api/?order-by=username,desc'
``` 

note : pour les trie on utilise la , comme séparateur entre le champ et le sens puis le ; pour séparer les champs : `?order-by=field1,asc|desc;field2sc|desc`

- fetch One :

``` 
curl --insecure GET -H 'Accept: application/hal+json'  -H 'Authorization: Basic [TOKEN]' 'https://localhost:8443/api/user/1'
``` 

- Post One :
- 
``` 
curl -X POST -k -H 'Authorization: Basic bWV0ZW9TSTptZXRlb1NJ' -H 'Content-Type: application/json' -H 'Accept: application/json' -i https://localhost:8443/api/user/1 -d '{"username":"test", "displayName":"User test"}
``` 

- Post multiple :
``` 
curl -X POST -k -H 'Authorization: Basic bWV0ZW9TSTptZXRlb1NJ' -H 'Content-Type: application/json' -H 'Accept: application/json' -i https://localhost:8443/api/user/1 -d '[{"username":"test", "displayName":"User test"}, {"username":"test2", "displayName":"User test 2"}]'
``` 


- Update One : 

``` 
curl --insecure PATCH -H 'Accept: application/hal+json' -H 'Content-Type: application/json' -H 'Authorization: Basic [TOKEN]' 'https://localhost:8443/api/user/1' -d '{"email":"newMail@mail.fr}'
``` 

- Update multiple : 

``` 
curl --insecure PATCH -H 'Accept: application/hal+json' -H 'Content-Type: application/json' -H 'Authorization: Basic [TOKEN]' 'https://localhost:8443/api/user/1' -d '[{"id": 1, "email":"test@mail.fr"},{"id": 2, "email":"new@mail.fr"}]'
``` 

- Delete One : 

``` 
curl --insecure DELETE -h 'Accept: application/hal+json' -h 'Authorization: Basic [TOKEN]' 'https://localhost:8443/api/user/1'
``` 

- Delete Multiple : 

``` 
curl --insecure DELETE -H 'Accept: application/hal+json' -H 'Content-Type: application/json' -H 'Authorization: Basic [TOKEN]' 'https://localhost:8443/api/user/1' -d '[{"id": 666},{"id": 667}]'
``` 