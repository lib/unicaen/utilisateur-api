-- Accés a l'API
INSERT INTO UNICAEN_PRIVILEGE_CATEGORIE (
    CODE,
    LIBELLE,
    NAMESPACE,
    ORDRE
)
values
    ('administration',           'Administration',                    'Application\Provider\Privilege', 1001)
  ON CONFLICT (CODE) DO
UPDATE SET
    LIBELLE=excluded.LIBELLE,
    NAMESPACE=excluded.NAMESPACE,
    ORDRE=excluded.ORDRE;

WITH d(code, lib, ordre) AS (
    SELECT 'api-administration',    'Administrer l''API',                   1 UNION
    SELECT 'api-documentation',     'Consulter la documentation de l''API', 2
)
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
         JOIN unicaen_privilege_categorie cp ON cp.CODE = 'administration'
    ON CONFLICT (CATEGORIE_ID, CODE) DO
UPDATE SET
    LIBELLE=excluded.LIBELLE,
    ORDRE=excluded.ORDRE;

-- on donne par défaut ces privilége à l'administrateur
INSERT INTO unicaen_privilege_privilege_role_linker
(role_id, privilege_id)
SELECT role.id, privilege.id
FROM
    unicaen_utilisateur_role role,
    unicaen_privilege_privilege privilege
        JOIN unicaen_privilege_categorie cp ON privilege.categorie_id = cp.id
WHERE role.role_id = 'Administrateur'
  AND cp.code = 'administration'
    ON conflict do nothing;
