<?php

namespace Api\V1\Entity\Db;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class User
{
    /**
     * @var int|null $id
     */
    protected ?int $id=null;

    /**
     * @var string|null $username
     */
    protected ?string $username=null;

    /**
     * @var string|null $email
     */
    protected ?string $email=null;

    /**
     * @var string|null $displayName
     */
    protected ?string $displayName=null;

    /**
     * @var bool|null $state
     */
    protected bool $state = true;


    /**
     * @var \Api\V1\Entity\Db\Role|null
     */
    protected ?Role $lastRole=null;

    /**
     * @var ArrayCollection
     */
    protected Collection $roles;

    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }
    /**
     * Get id.
     *
     * @return ?int
     */
    public function getId() :  ?int
    {
        return $this->id;
    }

    /**
     * Set id.
     *
     * @param ?int $id
     * @return User
     */
    public function setId( ?int $id) : self
    {
        $this->id = (int) $id;

        return $this;
    }

    /**
     * Get username.
     *
     * @return ?string
     */
    public function getUsername() :  ?string
    {
        return $this->username;
    }

    /**
     * Set username.
     *
     * @param ?string $username
     * @return User
     */
    public function setUsername( ?string $username) : self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()  : ?string
    {
        return $this->email;
    }

    /**
     * Set email.
     *
     * @param string|null $email
     * @return User
     */
    public function setEmail($email) : self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get displayName.
     *
     * @return string|null
     */
    public function getDisplayName() : ?string
    {
        return $this->displayName;
    }

    /**
     * Set displayName.
     *
     * @param string|null $displayName
     * @return User
     */
    public function setDisplayName(?string $displayName) : self
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get state.
     *
     * @return bool
     */
    public function getState() : bool
    {
        return $this->state;
    }

    /**
     * Set state.
     *
     * @param bool $state
     * @return User
     */
    public function setState(bool $state) : self
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get lastRole.
     *
     * @return Role|null
     */
    public function getLastRole() : ?Role
    {
        return $this->lastRole;
    }

    /**
     * Set lastRole.
     *
     * @param Role|null $lastRole
     * @return User
     */
    public function setLastRole(?Role $lastRole) : self
    {
        $this->lastRole = $lastRole;

        return $this;
    }

    /**
     * Get roles of user
     *
     * @return Collection
     */
    public function getRoles() : Collection
    {
        return $this->roles;
    }

    /**
     * Add a role to user.
     *
     * @param Role $role
     * @return User
     */
    public function addRole(Role $role) : self
    {
        $this->roles->add($role);
        return $this;
    }

    /**
     * Remove a role to user.
     *
     * @param Role $role
     * @return \Api\V1\Entity\Db\User
     *
     */
    public function removeRole(Role $role) : self
    {
        $this->roles->removeElement($role);

        return $this;
    }
}

