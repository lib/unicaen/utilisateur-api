<?php

namespace Api\V1\Entity\Db;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 *
 */
class RoleLight{

    /**
     * @var int
     */
    private int $id;

    /**
     * @var string
     */
    protected string $code;

    /**
     * @var string|null
     */
    protected ?string $libelle = null;

    /**
     * @var string|null
     */
    protected ?string $description = null;

    /**
     * @var bool
     */
    protected bool $auto = false;

    /**
     * @var \Api\V1\Entity\Db\RoleLight|null
     */
    protected ?RoleLight $parent = null;

    /**
     * @var \Doctrine\Common\Collections\Collection|\Doctrine\Common\Collections\ArrayCollection
     */
    protected Collection $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return void
     */
    public function setId(int $id) : void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCode() : string
    {
        return $this->code;
    }

    /**
     * @param string|null $code
     * @return void
     */
    public function setCode(?string $code) : void
    {
        $this->code = (string)$code;
    }

    /**
     * @return string|null
     */
    public function getLibelle() : ?string
    {
        return $this->libelle;
    }

    /**
     * @param string|null $libelle
     * @return void
     */
    public function setLibelle(?string $libelle) : void
    {
        $this->libelle = $libelle;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return void
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return bool
     */
    public function isAuto() : bool
    {
        return $this->auto;
    }

    /**
     * @param bool $auto
     * @return void
     */
    public function setAuto(bool $auto) : void
    {
        $this->auto = $auto;
    }

    /**
     * @return \Api\V1\Entity\Db\RoleLight|null
     */
    public function getParent() : ?RoleLight
    {
        return $this->parent;
    }

    /**
     * @param \Api\V1\Entity\Db\RoleLight|null $parent
     * @return void
     */
    public function setParent(?RoleLight $parent = null) : void
    {
        $this->parent = $parent;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers() : Collection
    {
        return $this->users;
    }

    /**
     * @param \Api\V1\Entity\Db\UserLight $user
     * @return void
     */
    public function addUser(UserLight $user) : void
    {
        $this->users->add($user);
    }

    /**
     * @param \Api\V1\Entity\Db\UserLight $user
     * @return void
     */
    public function removeUser(UserLight $user) : void
    {
        $this->users->removeElement($user);
    }
}
