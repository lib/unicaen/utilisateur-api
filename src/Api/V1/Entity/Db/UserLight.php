<?php

namespace Api\V1\Entity\Db;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class UserLight
{
    /**
     * @var int
     */
    protected int $id;

    /**
     * @var string
     */
    protected string $username;

    /**
     * @var string|null
     */
    protected ?string $email;

    /**
     * @var string|null
     */
    protected ?string $displayName;

    /**
     * @var bool
     */
    protected bool $state;


    /**
     * @var RoleLight
     */
    protected ?RoleLight $lastRole;

    /**
     * @var ArrayCollection
     */
    protected Collection $roles;

    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }
    /**
     * Get id.
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * Set id.
     *
     * @param int $id
     * @return RoleLight
     */
    public function setId(int $id) : self
    {
        $this->id = (int) $id;

        return $this;
    }

    /**
     * Get username.
     *
     * @return string
     */
    public function getUsername() : string
    {
        return $this->username;
    }

    /**
     * Set username.
     *
     * @param string $username
     * @return RoleLight
     */
    public function setUsername(string $username) : self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()  : ?string
    {
        return $this->email;
    }

    /**
     * Set email.
     *
     * @param string|null $email
     * @return RoleLight
     */
    public function setEmail($email) : self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get displayName.
     *
     * @return string|null
     */
    public function getDisplayName() : ?string
    {
        return $this->displayName;
    }

    /**
     * Set displayName.
     *
     * @param string|null $displayName
     * @return UserLight
     */
    public function setDisplayName(?string $displayName) : self
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get state.
     *
     * @return bool
     */
    public function getState() : bool
    {
        return $this->state;
    }

    /**
     * Set state.
     *
     * @param bool $state
     * @return UserLight
     */
    public function setState(bool $state) : self
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get lastRole.
     *
     * @return RoleLight|null
     */
    public function getLastRole() : ?RoleLight
    {
        return $this->lastRole;
    }

    /**
     * Set lastRole.
     *
     * @param RoleLight|null $lastRole
     * @return UserLight
     */
    public function setLastRole(?RoleLight $lastRole) : self
    {
        $this->lastRole = $lastRole;

        return $this;
    }

    /**
     * Get roles of user
     *
     * @return Collection
     */
    public function getRoles() : Collection
    {
        return $this->roles;
    }

    /**
     * Add a role to user.
     *
     * @param RoleLight $role
     * @return UserLight
     */
    public function addRole(RoleLight $role) : self
    {
        $this->roles->add($role);
        return $this;
    }

    /**
     * Remove a role to user.
     *
     * @param RoleLight $role
     * @return UserLight
     *
     */
    public function removeRole(RoleLight $role) : self
    {
        $this->roles->removeElement($role);

        return $this;
    }
}

