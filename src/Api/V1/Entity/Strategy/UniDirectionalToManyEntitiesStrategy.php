<?php

namespace Api\V1\Entity\Strategy;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Laminas\Hydrator\Strategy\AbstractCollectionStrategy;
use Laminas\ApiTools\Hal\Collection;

//class UniDirectionalToManyEntitiesStrategy extends AllowRemoveByValue
/** On ne peut plus hérité de AllowRemoveByValue qui est devenu Finale...*/
class UniDirectionalToManyEntitiesStrategy extends AbstractCollectionStrategy
{
    public function extract($value, ?object $object = null)
    {
        return new Collection($value ?: []);
    }

    /**
     * From AllowRemoveByValue
     */
    public function hydrate($value, ?array $data)
    {
        // AllowRemove strategy need "adder" and "remover"
        $adder   = 'add' . $this->getInflector()->classify($this->getCollectionName());
        $remover = 'remove' . $this->getInflector()->classify($this->getCollectionName());
        $object  = $this->getObject();

        if (! method_exists($object, $adder) || ! method_exists($object, $remover)) {
            throw new LogicException(
                sprintf(
                    'AllowRemove strategy for DoctrineModule hydrator requires both %s and %s to be defined in %s
                     entity domain code, but one or both seem to be missing',
                    $adder,
                    $remover,
                    $object::class,
                ),
            );
        }

        $collection = $this->getCollectionFromObjectByValue();
        $collection = $collection->toArray();

        $toAdd    = new ArrayCollection(array_udiff($value, $collection, [$this, 'compareObjects']));
        $toRemove = new ArrayCollection(array_udiff($collection, $value, [$this, 'compareObjects']));

        $object->$adder($toAdd);
        $object->$remover($toRemove);

        return $collection;
    }

}