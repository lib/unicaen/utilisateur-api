<?php

namespace Api\V1\Entity\Strategy;

use Doctrine\Laminas\Hydrator\Strategy\AbstractCollectionStrategy;

class UniDirectionalToManyIdsStrategy extends AbstractCollectionStrategy
{
    //Asssocie l'id de l'objet pas l'objet lui même
    public function extract($value, ?object $object = null)
    {
        $ids = $value->map(function ($v) {
            return $v->getId();
        });

        return $ids->toArray();
    }

    public function hydrate($value, ?array $data) {}
}