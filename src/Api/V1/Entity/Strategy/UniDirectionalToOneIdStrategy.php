<?php

namespace Api\V1\Entity\Strategy;

use Doctrine\Laminas\Hydrator\Strategy\AbstractCollectionStrategy;

class UniDirectionalToOneIdStrategy extends AbstractCollectionStrategy
{
    //Asssocie l'id de l'objet pas l'objet lui même
    public function extract($value, ?object $object = null)
    {
        return $value ? $value->getId() : null;
    }

    public function hydrate($value, ?array $data) {}
}