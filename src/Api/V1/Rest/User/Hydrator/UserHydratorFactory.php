<?php
namespace Api\V1\Rest\User\Hydrator;

use Api\V1\Entity\Strategy\UniDirectionalToManyEntitiesStrategy;
use Doctrine\ORM\EntityManager;
use Interop\Container\Containerinterface;
use Laminas\ServiceManager\Factory\FactoryInterface;


class UserHydratorFactory implements FactoryInterface
{
    /**
     * Create hydrator for service rest
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var \Api\V1\Rest\User\Hydrator\UserHydrator $hydrator */
        $hydrator = new UserHydrator();

        /** @var EntityManager $entityManager */
        $entityManager = $container->get(EntityManager::class);
        $hydrator->setEntityManager($entityManager);

        $strategy = $container->get(UniDirectionalToManyEntitiesStrategy::class);
        $hydrator->addStrategy('roles', $strategy);
        return $hydrator;
    }
}