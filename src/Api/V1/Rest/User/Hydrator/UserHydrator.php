<?php
namespace Api\V1\Rest\User\Hydrator;

use Api\V1\Entity\Db\Role;
use Api\V1\Entity\Db\User;
use Exception;
use Laminas\Hydrator\AbstractHydrator;
use Laminas\Hydrator\HydratorInterface;
use UnicaenApp\Service\EntityManagerAwareTrait;

class UserHydrator extends AbstractHydrator implements HydratorInterface
{
    use EntityManagerAwareTrait;

    public function extract(object $object): array
    {
        if(!$object instanceof User){
            throw new Exception("Impossible d'extraire un objet autre qu'un User");
        }
        $data = [];
        $data['id'] = $object->getId();
        $data['username'] = $object->getUsername();
        $data['displayName'] = $object->getDisplayName();
        $data['email'] = $object->getEmail();
        $data['lastRole'] = $object->getLastRole();
        $strategy = $this->getStrategy('roles');
        $data['roles'] = $strategy->extract($object->getRoles());
        return $data;
    }

    public function hydrate(array $data, object $object)
    {
        if(!$object instanceof User){
            throw new Exception("Impossible d'hydrater un objet autre qu'un User");
        }
        if(isset($data['id'])){$object->setId($data['id']);}
        if(isset($data['username'])){$object->setUsername($data['username']);}
        if(isset($data['displayName'])){$object->setDisplayName($data['displayName']);}
        if(isset($data['email'])){$object->setEmail($data['email']);}
//        On n'autorise pas a changer le lastRole car celui-ci est automatique
//        if(isset($data['lastRole'])){$object->setLastRole($data['lastRole']);}

        return $object;
    }
}
