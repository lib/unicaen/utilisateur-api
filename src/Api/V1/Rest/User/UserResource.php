<?php
namespace Api\V1\Rest\User;

use Api\V1\Doctrine\DoctrineResource; //Versions autorisant l'ajout multiple
//use Laminas\ApiTools\Doctrine\Server\Resource\DoctrineResource;

use Laminas\ApiTools\ApiProblem\ApiProblem;
use Laminas\ApiTools\Doctrine\Server\Event\DoctrineResourceEvent;
class UserResource extends DoctrineResource
{
    /** Méthode a redéfinir si l'on veux modifier l'accés aux à l'entité */
    public function fetch($id)
    {
        return parent::fetch($id);
    }

    /**
     * Méthode a redéfinir si l'on veux modifier l'ajout d'un user via un appel en POST
     * Basé sur la méthode parent mais sans les modification en BDD
     * */
    public function create($data)
    {
        $entityClass = $this->getEntityClass();

        $data = $this->getQueryCreateFilter()->filter($this->getEvent(), $entityClass, $data);
        if ($data instanceof ApiProblem) {
            return $data;
        }

        $entity =  new $entityClass();

        $results = $this->triggerDoctrineEvent(DoctrineResourceEvent::EVENT_CREATE_PRE, $entity, $data);
        if ($results->last() instanceof ApiProblem) {
            return $results->last();
        } elseif (! $results->isEmpty() && $results->last() !== null) {
            // TODO Change to a more logical/secure way to see if data was acted and and we have the expected response
            $preEventData = $results->last();
        } else {
            $preEventData = $data;
        }
        $hydrator = $this->getHydrator();
        $hydrator->hydrate((array) $preEventData, $entity);

//        $this->getObjectManager()->persist($entity);
//
        $results = $this->triggerDoctrineEvent(DoctrineResourceEvent::EVENT_CREATE_POST, $entity, $data);
        if ($results->last() instanceof ApiProblem) {
            return $results->last();
        }

//        $this->getObjectManager()->flush();
//Ajout d'un faut id pour les test
//        $entity->setId(0);
        return $entity;
    }

    /**
     * Méthode a redéfinir si l'on veux modifier l'entité via un appel en PATCH
     **/
    public function patch($id, $data)
    {
        //Sécurité pour éviter de modifier des informations "critique"
        if(isset($data->id)){unset($data->id);}
        if(isset($data->username)){unset($data->username);}
        return parent::patch($id, $data);
    }

    /**
     * Modification multiple
     **/
    public function patchList($data)
    {
        return parent::patchList($data);
    }

    /** Méthode a redéfinir si l'on veux modifier la suppression
     * C'est le cas ici, car pour l'exemple on fait une méthode delete mais que l'on ne souhaite pas qu'elle supprime réelement
     * On a ici copier la méthode delete de DoctrineResource mais en ne faisant pas appel au remove et au flush
     */
    public function delete($id)
    {
        $entity = $this->findEntity($id, 'delete');
        if ($entity instanceof ApiProblem) {
            return $entity;
        }

        $results = $this->triggerDoctrineEvent(DoctrineResourceEvent::EVENT_DELETE_PRE, $entity);
        if ($results->last() instanceof ApiProblem) {
            return $results->last();
        }

//        $this->getObjectManager()->remove($entity);

        $results = $this->triggerDoctrineEvent(DoctrineResourceEvent::EVENT_DELETE_POST, $entity);
        if ($results->last() instanceof ApiProblem) {
            return $results->last();
        }
//        $this->getObjectManager()->flush();
//         throw new Exception("Suppression désactivée volontairement");
        return true;
    }

    public function deleteList($data)
    {
     return parent::deleteList($data);
    }
}
