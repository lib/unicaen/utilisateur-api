<?php

namespace Api\V1\Rest\User;

use Api\V1\Query\Provider\AbstractQueryProvider;
use Laminas\ApiTools\Rest\ResourceEvent;

class UserFetchAll extends AbstractQueryProvider
{
    /**
     * @param ResourceEvent $event
     * @param string $entityClass
     * @param array $parameters
     * @return mixed|null
     */
    public function createQuery(ResourceEvent $event, $entityClass, $parameters)
    {
        parent::createQuery($event, $entityClass, $parameters);

        return parent::makeQuery($event, $entityClass, $parameters, $filter ?? []);
    }
}