<?php

namespace Api\V1\Rest\Role;

use Doctrine\ORM\EntityManager;
use Laminas\ApiTools\Doctrine\QueryBuilder\Filter\Service\ORMFilterManager;
use Laminas\ApiTools\Doctrine\QueryBuilder\OrderBy\Service\ORMOrderByManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class RoleLightFetchAllFactory
{
    /**
     * @param ContainerInterface $container
     * @param $requestedName
     * @param array|null $options
     * @return RoleLightFetchAll
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /**
         * @var EntityManager $entityManager
         * @var ORMFilterManager $filterManager
         * @var ORMOrderByManager $orderByManager
         */
        $entityManager = $container->get('Doctrine\ORM\EntityManager');
        $filterManager = $container->get(ORMFilterManager::class);
        $orderByManager = $container->get(ORMOrderByManager::class);

        $instance = new RoleLightFetchAll();
        $instance->setObjectManager($entityManager);
        $instance->setFilterManager($filterManager);
        $instance->setOrderByManager($orderByManager);

        return $instance;
    }
}
