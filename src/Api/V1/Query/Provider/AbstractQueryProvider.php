<?php

namespace Api\V1\Query\Provider;

use Doctrine\ORM\QueryBuilder;
//use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\ApiTools\Doctrine\Server\Query\Provider\AbstractQueryProvider as LaminasApiToolsAbstractQueryProvider;
use Laminas\ApiTools\ApiProblem\ApiProblem;
use Laminas\ApiTools\Doctrine\QueryBuilder\Filter\Service\ORMFilterManager;
use Laminas\ApiTools\Doctrine\QueryBuilder\OrderBy\Service\ORMOrderByManager;
use Laminas\ApiTools\Rest\ResourceEvent;

abstract class AbstractQueryProvider extends LaminasApiToolsAbstractQueryProvider
{
//    use ProvidesObjectManager;

    /**
     * @var ORMFilterManager $filterManager
     */
    protected $filterManager;

    /**
     * @var ORMOrderByManager $orderByManager
     */
    protected $orderByManager;

    /**
     * @var QueryBuilder $queryBuilder
     */
    protected $queryBuilder;

    /**
     * Liste des paramètres GET par défaut autorisés dans la requête
     *
     * @var array
     */
    protected $filterParameters = [
        'page', // nécessaire en cas de mise en place de la pagination
    ];


    /**
     * @return ORMFilterManager
     */
    public function getFilterManager()
    {
        return $this->filterManager;
    }

    /**
     * @param ORMFilterManager $filterManager
     * @return $this
     */
    public function setFilterManager(ORMFilterManager $filterManager)
    {
        $this->filterManager = $filterManager;

        return $this;
    }

    /**
     * @return ORMOrderByManager
     */
    public function getOrderByManager()
    {
        return $this->orderByManager;
    }

    /**
     * @param ORMOrderByManager $orderByManager
     * @return $this
     */
    public function setOrderByManager(ORMOrderByManager $orderByManager)
    {
        $this->orderByManager = $orderByManager;

        return $this;
    }

    /**
     * @return QueryBuilder
     */
    public function getQueryBuilder()
    {
        return $this->queryBuilder;
    }

    /**
     * @param ResourceEvent $event
     * @param string $entityClass
     * @param array $parameters
     * @return mixed This will return an ORM or ODM Query\Builder
     */
    public function createQuery(ResourceEvent $event, $entityClass, $parameters)
    {
        $this->queryBuilder = $this->getObjectManager()->createQueryBuilder();
        $this->queryBuilder
            ->select('row')
            ->from($entityClass, 'row');

        return $this->queryBuilder;
    }

    /**
     * @param ResourceEvent $event
     * @param string $entityClass
     * @param array $parameters
     * @param array $filter
     * @return mixed This will return an ORM or ODM Query\Builder
     */
    public function makeQuery(ResourceEvent $event, $entityClass, $parameters, $filter)
    {
        $request = $event->getRequest()->getQuery()->toArray();
        foreach($request as $param => $value) {
            if(!in_array($param, array_merge($this->filterParameters, array_keys($parameters)))) {
                return new ApiProblem (400, "Not authorized parameter '$param' in URI");
            }
        }

        if (!empty($filter)) {
            $metadata = $this->getObjectManager()->getClassMetadata($entityClass);
            $this->getFilterManager()->filter(
                $this->queryBuilder,
                $metadata,
                $filter
            );
        }

        if (isset($parameters['order-by'])) {
            foreach (explode(';', $parameters['order-by']) as $oby) {
                list($field, $direction) = explode(',', $oby, 2);

                if (null == $field) {
                    continue;
                }

                if (null == $direction || !in_array($direction, ['asc', 'desc'])) {
                    $direction = 'asc';
                }

                $orderBy[] = [
                    'type' => 'field',
                    'field' => $field,
                    'direction' => $direction
                ];
            }

            if ($orderBy) {
                $metadata = $this->getObjectManager()->getClassMetadata($entityClass);
                $this->getOrderByManager()->orderBy(
                    $this->queryBuilder,
                    $metadata,
                    $orderBy
                );
            }
        }

//        var_dump($this->queryBuilder->getDQL());
        return $this->queryBuilder;
    }
}