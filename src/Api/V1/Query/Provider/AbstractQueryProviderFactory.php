<?php

namespace Api\V1\Query\Provider;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\AbstractFactoryInterface;
use Doctrine\Instantiator\Instantiator;
use Laminas\ApiTools\Doctrine\QueryBuilder\Filter\Service\ORMFilterManager;
use Laminas\ApiTools\Doctrine\QueryBuilder\OrderBy\Service\ORMOrderByManager;
use Psr\Container\NotFoundExceptionInterface;

class AbstractQueryProviderFactory implements
    AbstractFactoryInterface
{
    public function canCreate(ContainerInterface $container, $requestedName)
    {
        $instantiator = new Instantiator();
        $instance = $instantiator->instantiate($requestedName);

        return ($instance instanceof AbstractQueryProvider);
    }

    /**
     * @param ContainerInterface $container
     * @param $requestedName
     * @param array|null $options
     * @return AbstractQueryProvider
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /**
         * @var EntityManager $entityManager
         * @var ORMFilterManager $filterManager
         * @var ORMOrderByManager $orderByManager
         */
        $entityManager = $container->get('Doctrine\ORM\EntityManager');
        $filterManager = $container->get(ORMFilterManager::class);
        $orderByManager = $container->get(ORMOrderByManager::class);

        $instance = new $requestedName();
        $instance->setObjectManager($entityManager);
        $instance->setFilterManager($filterManager);
        $instance->setOrderByManager($orderByManager);

        return $instance;
    }
}