<?php
namespace  Api\V1\Doctrine;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Laminas\ApiTools\ApiProblem\ApiProblem;
use Laminas\ApiTools\Doctrine\Server\Exception\InvalidArgumentException;
use Laminas\ApiTools\Rest\ResourceEvent;
use Laminas\EventManager\EventManagerInterface;

/**
 * @desc Surcharge de DoctrineRessource permettant de gerer les creation multiple depuis l'API
 * Par défaut DoctrineRessource de Laminas\ApiTools ... ne semble pas le proposé
 * TODO modifier AbstractRestfulController pour qu'il distingue l'ajout d'une entité ou de plusieur et qu'il génére le bonne Event
 * La solution ici n'est qu'un contournement du problème.
 */
class DoctrineResource extends \Laminas\ApiTools\Doctrine\Server\Resource\DoctrineResource
{
    const EVENT_CREATE_LIST_PRE = "create-list.pre";
    const EVENT_CREATE_LIST_POST = "create-list.post";

    public function createList($data){
        $return = new ArrayCollection();

        $results = $this->triggerDoctrineEvent(self::EVENT_CREATE_LIST_PRE, $data, $data);
        if ($results->last() instanceof ApiProblem) {
            return $results->last();
        }

        if (! $this->getObjectManager() instanceof EntityManagerInterface) {
            throw new InvalidArgumentException('Invalid Object Manager, must implement EntityManagerInterface');
        }

        $this->getObjectManager()->getConnection()->beginTransaction();
        foreach ($data as $row) {
            $result = $this->create($row);
            if ($result instanceof ApiProblem) {
                $this->getObjectManager()->getConnection()->rollback();

                return $result;
            }
            $return->add($result);
        }
        $this->getObjectManager()->getConnection()->commit();

        $results = $this->triggerDoctrineEvent(self::EVENT_CREATE_LIST_POST, $return, $data);
        if ($results->last() instanceof ApiProblem) {
            return $results->last();
        }
        return $return;
    }

    /**
     * Attach listeners for all Resource events
     * simule l'ajout d'une métode createList  *
     * On ne fait ici que simuler l'ajout de l'event en question
     * @param int $priority
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->attach('create', [$this, 'dispatch']);
        $this->listeners[] = $events->attach('createList', [$this, 'dispatch']);
        $this->listeners[] = $events->attach('delete', [$this, 'dispatch']);
        $this->listeners[] = $events->attach('deleteList', [$this, 'dispatch']);
        $this->listeners[] = $events->attach('fetch', [$this, 'dispatch']);
        $this->listeners[] = $events->attach('fetchAll', [$this, 'dispatch']);
        $this->listeners[] = $events->attach('patch', [$this, 'dispatch']);
        $this->listeners[] = $events->attach('patchList', [$this, 'dispatch']);
        $this->listeners[] = $events->attach('replaceList', [$this, 'dispatch']);
        $this->listeners[] = $events->attach('update', [$this, 'dispatch']);
    }

    /**
     * En cas d'event de type Create, on distingue le create Unique du CreateList en regardant si les données sont un tableaux ou une entité
     * @return mixed
     */
    public function dispatch(ResourceEvent $event)
    {
        $this->event = $event;
        switch ($event->getName()){
            case 'create':
                $data = $event->getParam('data', []);
                $data=(array)$data;
                if(empty($data)){
                    return false;
                }
                $first = current($data);
                if(is_array($first)){
                    return $this->createList($data);
                }
                else{
                    return $this->create($data);
                }
            default : return parent::dispatch($event);
        }
    }

}
