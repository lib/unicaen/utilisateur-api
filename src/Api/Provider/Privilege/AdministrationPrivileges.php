<?php

namespace Api\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class AdministrationPrivileges extends Privileges
{
    const API_ADMINISTRATION            = 'administration-api-administration';
    const API_DOCUMENTATION             = 'administration-api-documentation';
}