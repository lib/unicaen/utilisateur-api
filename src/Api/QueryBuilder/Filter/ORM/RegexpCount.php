<?php

namespace Api\QueryBuilder\Filter\ORM;

use Doctrine\ORM\Query\Expr\Comparison;
use Doctrine\ORM\Query\Expr\Func;
use Laminas\ApiTools\Doctrine\QueryBuilder\Filter\ORM\AbstractFilter;

class RegexpCount extends AbstractFilter
{
    public function filter($queryBuilder, $metadata, $option)
    {
        if (isset($option['where'])) {
            if ($option['where'] === 'and') {
                $queryType = 'andWhere';
            } elseif ($option['where'] === 'or') {
                $queryType = 'orWhere';
            }
        }

        if (! isset($queryType)) {
            $queryType = 'andWhere';
        }

        if (! isset($option['alias'])) {
            $option['alias'] = 'row';
        }

        $func = new Func(
            'REGEXP_COUNT', [
                $option['alias'] . '.' . $option['field'],
                $queryBuilder->expr()->literal($option['value']),
            ]
        );
        $comparison = new Comparison($func, '!=', 0);

        $queryBuilder->$queryType($comparison);
    }
}
