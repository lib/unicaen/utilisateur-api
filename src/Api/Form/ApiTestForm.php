<?php

namespace Api\Form;

use Laminas\Form\Element;
use Laminas\Form\Form;

class ApiTestForm extends Form
{
    public function init()
    {
        $this->setAttribute('method', 'get');

        $this->add([
            'type' => Element\Select::class,
            'name' => 'service',
            'options' => [
                'label' => "Nom du service :",
                'empty_option' => "Sélectionner un service...",
                'value_options' => [
                    'compte' => [
                        'label' => "COMPTE",
                        'options' => [
                            'individu-compte' => "Les comptes",
                            'individu-compte-statut' => "Les statuts du compte",
                            'individu-compte-type' => "Les types de compte",
                        ]
                    ],
                    'fonction' => [
                        'label' => "FONCTION",
                        'options' => [
                            'fonction' => "Les fonctions",
                            'fonction-type' => "Les types de fonction",
                        ],
                    ],
                    'immobilier' => [
                        'label' => "IMMOBILIER",
                        'options' => [
                            'immobilier-site' => "Les sites",
                            'immobilier-batiment' => "Les bâtiments",
                            'immobilier-niveau' => "Les niveaux",
                            'immobilier-local' => "Les locaux",
                        ]
                    ],
                    'individu' => [
                        'label' => "INDIVIDU",
                        'options' => [
                            'individu' => "Les individus",
                            'individu-adresse-type' => "Les types d'adresse postale",
                            'individu-affectation-type' => "Les types d'affectation"
                        ],
                    ],
                    'localisation' => [
                        'label' => "LOCALISATION",
                        'options' => [
                            'pays' => "Les pays",
                        ],
                    ],
                    'structure' => [
                        'label' => "STRUCTURE",
                        'options' => [
                            'structure' => "Les structures",
                            'structure-type' => "Les types de structure",
                            'structure-recherche-type' => "Les types de structure recherche",
                        ],
                    ],
                    'telephone' => [
                        'label' => "TÉLÉPHONE",
                        'options' => [
                            'telephone-type' => "Les types de téléphone",
                        ],
                    ]
                ],
            ],
            'attributes' => [
                'id' => 'service',
                'title' => "Sélectionner un service...",
                'class' => 'bootstrap-selectpicker show-tick'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'url',
            'options' => [
                'label' => "Url du service :",
            ],
            'attributes' => [
                'id' => 'url',
            ],
        ]);

        $this->add([
            'type' => Element\Button::class,
            'name' => 'envoyer',
            'options' => [
                'label' => '<i class="fas fa-arrow-circle-right"></i> Envoyer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'id' => "envoyer",
                'type' => 'button',
                'class' => 'btn btn-success',
                'disabled' => 'disabled',
            ],
        ]);
    }
}