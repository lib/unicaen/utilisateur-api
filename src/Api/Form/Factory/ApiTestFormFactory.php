<?php

namespace Api\Form\Factory;

use Api\Form\ApiTestForm;
use Psr\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ApiTestFormFactory implements FactoryInterface
{
    /**
     * Create form
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ApiTestForm|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new ApiTestForm();
        return $form;
    }
}