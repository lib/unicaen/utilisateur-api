<?php

namespace Api\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class DocumentationController extends AbstractActionController
{
    public function definitionYamlAction()
    {
        $api = $this->params()->fromRoute('api') ?: 'Api';
        $version = $this->params()->fromRoute('version') ?: 1;
        $response = $this->getResponse();

        $response->setContent($content = file_get_contents(sprintf('./module/%s/config/api-v%s.yml', $api, $version)));
        $f = finfo_open();
        $mime_type = finfo_buffer($f, $content, FILEINFO_MIME_TYPE);
        $response
            ->getHeaders()
            ->addHeaderLine('Content-Transfer-Encoding', 'binary')
            ->addHeaderLine('Content-Type', $mime_type)
            ->addHeaderLine('Content-length', strlen($content));

        return $response;
    }

    /**
     * Show the Swagger UI for a given API
     *
     * @return ViewModel
     */
    public function afficherAction()
    {
        $api = $this->params()->fromRoute('api') ?: 'Api';
        $version = $this->params()->fromRoute('version') ?: 1;
        $viewModel = new ViewModel(['api' => $api, 'version' => $version]);
        $viewModel->setTerminal(true);
        return $viewModel;
    }
}