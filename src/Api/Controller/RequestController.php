<?php

namespace Api\Controller;

use Api\Form\ApiTestForm;
use Laminas\Mvc\Controller\AbstractActionController;

class RequestController extends AbstractActionController
{
    const ACTION_TESTER = 'tester';

    /**
     * @var ApiTestForm
     */
    protected $apiTestForm;


    /**
     * @param ApiTestForm $form
     *
     * @return $this
     */
    public function setApiTestForm($form)
    {
        $this->apiTestForm = $form;

        return $this;
    }


    public function testerAction()
    {
       $form = $this->apiTestForm;

       return compact('form');
    }
}