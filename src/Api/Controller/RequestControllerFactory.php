<?php

namespace Api\Controller;

use Api\Form\ApiTestForm;
use Psr\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class RequestControllerFactory implements FactoryInterface
{
    /**
     * Create controller
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return RequestController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new RequestController();
        $controller->setApiTestForm($container->get('FormElementManager')->get(ApiTestForm::class));

        return $controller;
    }
}