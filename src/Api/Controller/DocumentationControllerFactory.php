<?php

namespace Api\Controller;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

class DocumentationControllerFactory implements FactoryInterface
{
    /**
     * Create controller
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DocumentationController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new DocumentationController();

        return $controller;
    }
}